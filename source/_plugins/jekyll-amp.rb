
module Jekyll
  class AmpPage < Jekyll::Page
    def initialize(site, base, dir, post)
      self.data = post.data.clone
      self.content = self.getContent(post, site)
      self.data['layout'] = 'amp'
      @site = site
      @base = base
      @dir = dir
      @url = dir
      @name = 'index.html'
      self.process(@name)
      self.data['body'] = self.content
      self.data = post.data.merge(self.data)
      self.data.delete('excerpt')
      self.data.delete('permalink')
      self.data['canonical_url'] = post.url
    end

    def getContent(page, site)
      raw = (Liquid::Template.parse page.content, :error_mode => :warn).render site.site_payload, :strict_variables => false, :strict_filters => false
      #Note about gsub: some jekyll versions ignore error_mode and strict_* options, so we need to remove that messages manually :(
      raw.gsub('Liquid error: internal','').gsub(/<script.*?>[\s\S]*<\/script>/i, "")
    end
  end

  class AmpGenerator < Generator
    priority :low
    def generate(site)
      site.pages.each do |post|
        next if !post['title'] or post['noindex'] or post['layout'] == 'amp'
        url = post['url'].gsub('.html', '')
        site.pages << AmpPage.new(site, site.source, File.join('amp', url), post)
      end
      site.posts.docs.each do |post|
        site.pages << AmpPage.new(site, site.source, File.join('amp', post.id + '.html'), post)
      end
    end
  end
end
