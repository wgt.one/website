---
layout: post
title: Next steps after survey
categories: news
---
Hey,<br>
WGT developer here.

I got interesting responses in features survey (previous blog post), so here is the list of next steps:

1. WGT will receive guild API for sure.
Right now, JSON is Top#1 language for it.
Some parts of api will be enabled by default (like guild config and guild roster),
but some of them will be part of patreon subscription (attendance and future "audit" module, $1 package and more).
2. WGT will receive ability to select twinks/alts from pre-defined guild roster for retail guilds
(again, sorry, but Blizzard doesn't expose api for classic guilds),
that will be implemented after the API step (because it requires api), thanks to Nàstrandir guild for the idea.
3. WGT will receive ability to host guild "homepages"
(like raiderIO or wowprogress, but with more control and ability to customize content)
for all both classic and retail guilds
(yep, that feature is needed by classic guilds mostly, but why not?).
It will be enabled by default, btw
4. finally, i'm investigating WarcraftLogs APIv2 and it will take some time to implement it,
so after guild pages i will dig deep into graphql integration and move WCL integration to next api version.

and about localization, seems nobody cares (literally 1 or 2 guilds interested in it),
so it will stay in "todo list", but with low priority (простите, ребятки).

PS: dicussion in [discord]({{ site.discord }})
