---
layout: post
title: Features Survey
categories: news
---

Hey hey!<br>
WGT developer here.

I have some ideas in mind, but I want to know if you interested in such features/functions or not.
Could you complete a survey, please? It will take less then 5-10 minutes (actually, it's only 4 questions and your guild name).

[Features Survey](https://form.typeform.com/to/hWqnC624)
