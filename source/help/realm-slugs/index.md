---
title: Realm names and slugs
category: help
layout: page
description: How to avoid erros when adding your guild
---
<div class="transparent-bg" markdown="1">
We use realm name/slug, provided by WarcraftLogs, so it is not standard names by Blizzard, some examples:

* `Азурегос` must be `азурегос`
* `Борейская тундра` must be `бореиская-тундра`
* `Вечная песня` must be `вечная-песня`
* `Галакронд` must be `галакронд`
* `Голдринн` must be `голдринн`
* `Гордунни` must be `гордунни`
* `Гром` must be `гром`
* `Дракономор` must be `дракономор`
* `Король Лич` must be `король-лич`
* `Пиратская бухта` must be `пиратская-бухта`
* `Пламегор` (flamegor) must be `пламегор`
* `Подземье` must be `подземье`
* `Разувий` must be `разувии`
* `Ревущий фьорд` must be `ревущии-фьорд`
* `Рок-Делар` must be `рокделар`
* `Свежеватель Душ` must be `свежеватель-душ`
* `Седогрив` must be `седогрив`
* `Страж Смерти` must be `страж-смерти`
* `Термоштепсель` must be `термоштепсель`
* `Ткач Смерти` must be `ткач-смерти`
* `Черный шрам` must be `черныи-шрам`
* `Ясеневый Лес` must be `ясеневыи-лес`
* and so on. Note on `й` and `и`.

**You can find correct realm name by opening WarcraftLogs website and searching for your guild**.

Open your guild from search results list and check browser address bar, it will be something like that:

* Russian example: `https://classic.warcraftlogs.com/guild/eu/рокделар/Сверхнова` - your realm is `рокделар`
* English example: `https://www.warcraftlogs.com/guild/eu/blackhand/Nàstrandir` - your realm is `blackhand`
</div>
