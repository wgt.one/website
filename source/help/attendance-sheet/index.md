---
title: Attendance sheet
layout: page
category: help
description: How attendance sheet works and how to configure it
---
<div class="transparent-bg" markdown="1">
WoW Guild Tools integrates with WarcraftLogs to get your public logs data.
Using special algorythms that data convertred to the WGT attendance sheet.

### What is "Att. by Night"?

Standard attendance percent: `(amount of visited by player raid nights / total count of all raid nights) * 100`

### What is "Att. by Pull"?
Attendance percent by pulls. If character was on raid night, but (for example) was late or was kicked, he will have less pulls
than other raid members, so formula here: `(amount of done pulls by player / total count of all pulls of all raid nights) * 100`

### What are numbers on raid night cells?

**If** player was on raid night, numbers will be "X/Y" where `X` - amount of pulls done by player, `Y` - total count of pulls per raid night

**If** player was not on raid night, number will be "0", because he did no pulls.

### What is "info"/history for raid night?

1. List of pulled bosses with difficulty, count of pulls and final status - killed or wiped
2. List of pulls done by each player per boss.

### How does alts matching work?

1. You have multiple characters: Etke, Sivy, Zirel
2. You add all these characters to our database and set "Etke" as main
3. Each time any of these characters will be found in logs, they will be replaced/merged with your main charcter (Etke)

### How does reports merging work?

1. You or guildmates uploaded multiple reports from one raid night
2. WGT will find multiple reports with the same date and fights
3. All characters from those reports will be parsed into one report
4. All pulls (**boss** fights) will be parsed into one report.
5. All character-fights relations will be parsed into one report.
6. As result, you will get one report, contains all data from multiple reports for that raid night.

### Usefull links

* [Attendance: bench](/help/attendance-bench)
</div>
