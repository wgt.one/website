---
title: "Troubleshooting"
layout: page
category: help
description: If something does not work as expected
---
<div class="transparent-bg" markdown="1">
### Empty attendance table on guild page

Check your guild configuration. Seems your filters are very strict. Check following options

* **Included** raids - may be you raid in Nathria, but not enabled it on WGT
* **Ignored** modes - may be you raid in Heroic, but disabled it on WGT
* **Ignored** weekdays - may be you raid on Friday, but disabled it on WGT

99% situations with empty table - filters are too strict.

### 0/0 cells in attendance table

Check your guild configuration. Seems your filters are very strict. Check following options

* **Included** raids - may be you raid in Nathria, but not enabled it on WGT
* **Ignored** modes - may be you raid in Heroic, but disabled it on WGT
* **Ignored** weekdays - may be you raid on Friday, but disabled it on WGT

99% situations with "0/0" cells in table - filters are too strict. _Yes, the same reasons as empty table_

### No link to guild page after 1 hour

* Check game version in your guild configuration, most common reason - wrong game version.
* Rare, but possible situation - WGT service problem, if game version is correct, ping developer in [discord]({{ site.discord }})
</div>
