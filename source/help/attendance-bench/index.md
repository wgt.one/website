---
title: "Attendance: bench"
layout: page
category: help
description: What is ''on the bench'' and how can you mark characters
---
<div class="transparent-bg" markdown="1">
### How does benching works?

You can set guildmate "on bench" for specific raid, common usecase: you have 20 raiders in group on mythic raid and 5 more "on the bench"
in case if one of raiders will have problems with internet or need to leave or just because one of raiders came here on specific bosses for loot.
In that case, you can mark raiders "on the bench" for that raid, so they will not loose their attendance (by night).

To do that,
1. [Login to your account](/account)
2. Go to your guild public page, eg: [{{site.url}}/tirisfal](/tirisfal)
3. Find an intersection (cell) between character and raid night column
4. That cell will be empty, click on it.
5. Now cell contains yellow "0" - which means that it marked as "on bench".
6. On next website update (every hour), your mark will be taken into calculations
and marked character will have attendance % for that raid night, even he didn't participate in any pull.

> **NOTE**: After site update, "on the bench" cell will change it's value to "0/XX", where XX is count of pulls of the whole raid night.
If you want to "un-bench" (remove mark), just click on that cell again. Your changes will be saved immediately, but you will still see
"on the bench" mark untill site update. Don't worry, it works as expected, just be patient :)
</div>
