---
title: Guild form
category: help
layout: page
description: How to add guild and what all those fields mean?!
---

<div class="transparent-bg" markdown="1">
## Add guild

To add new guild, go to [account](/account) and login or register,
after that you can use guild form to create new guild.

## Guild name

Your guild name, including all symbols, examples: `Æxøde`, `Nàstrandir`, `Schattenspiel`, `Совет Тирисфаля`

## Game

Supported games: WoW Retail, WoW Classic

## Realm

Your realm name "for urls" - slug. Like `howling-fjord`, [check out that page](/help/realm-slugs)

## Optional 3rdParty keys

Services, like WarcraftLogs or Blizzard, provides you limited ability to interract with their API.
If you generate more requests then allowed by service, you will be locked from service for some time.
WGT may get into that trouble, so if you want to speed up updates and avoid accidental pauses,
you may add your own keys below (note: keys used only to get public information about your guild,
no private info even touched).

### WarcraftLogs APIv1 Key

You may set your own WarcraftLogs APIv1 key to speed up your guild processing.
[Create for Retail](https://www.warcraftlogs.com/profile#api-title) or
[Create for Classic](https://classic.warcraftlogs.com/profile#api-title)

> **NOTE**: you must set API key name, otherwise it will not work (you may put anything you want here)

<!--
### WarcraftLogs APIv2 Credentials

WarcraftLogs APIv2 uses OAuth2 Client ID / Client Secret pair.

[Create for Retail](https://www.warcraftlogs.com/api/clients/) or
[Create for Classic](https://classic.warcraftlogs.com/api/clients/)

Put following params:

* **Enter a name for your application:** anything you want, but don't leave it empty
* **Enter one or more redirect URLs, separated by commas:** leave it empty
* **Public Client (Only the PKCE code flow will be usable). Check this if you would not be able to store a client secret securely:** do not check

### Blizzard API Credentials

1. Login on [Blizzard Developer Portal](https://dev.battle.net/)
2. [Create API Client](https://develop.battle.net/access/clients/create) with following information:

* **Client Name**: anything you want
* **Redirect URLs**: leave empty
* **I do not have a service URL for this client.**: check it
* **Intended Use**: write something, eg: Guilds audit, used in background job to get guild roster

create it and set Client ID and Client Secret to your guild form on WGT
-->

## Output reports

Maximum columns / raid nights in attendance table.
Note, output reports applied only to **output**, but actually WGT will parse `output reports * 2`
to use reports merging

**Advised value**: `10` - in that case 1 raid night = 10% attendance

## Threshold %

Attendance percent. If character has attendance less then that value, it will be removed from attendance table.

**Advised value**: `10` - in that case you will see "missing" people immediately and new raiders will be added fast.

## Allowed raids

Only selected raids will be included in attendance report, all other will be ignored.

## Auto "on the bench" mark

Like [manual "on the bench"](/help/attendance-bench), but automatic.

## Ignored modes

If you want to track only Mythic progress, you may select all other modes as ignored, so they will be removed from your attendance report.

## Ignored weekdays

If you want to track only specific days (eg: guild RTs on Wednesday and Friday),
you may select all other days as ignored, so logs reported on those days wil be ignored.

## Ignore before

If you want to ignore any reports before exact date, put it in that field.
It may be useful if you want to start new raid season or expansion and get rid of old data.

## Alts/twinks matching

That thing will merge your main and alts/twinks characters into 1 character, including overall attendance (by pull, too)

## Next run

WGT has website (frontend) that works 24/7 and you use it right now,
but log parsing, attendance calculations and a lot of other heavy stuff cannot be done on user browser.
That's why we have "backend" part which runs every hour and do all work, after that website updates with
new data from "backend".
</div>
