---
title: Public API
layout: page
category: help
description: How to get data from WGT
---
<div class="transparent-bg" markdown="1">


<!-- vim-markdown-toc GFM -->

- [About API](#about-api)
    + [Throttling](#throttling)
    + [Updates](#updates)
- [Guild](#guild)
    + [Example](#example)

<!-- vim-markdown-toc -->

### About API

Right now, API is very basic, just to provide ability to get data from WGT in machine-readable format
for your own needs.

#### Throttling

Not implemented. No plans to implement it. Use API as much as you need, just don't be a jerk.

#### Updates

API data updates with the whole website update [more info](/help/guild-form/#next-run)

### Guild

Any guild has `/api/{guildpath}/guild.json` that contains public guild configuration and guild roster (members),
go structure (check example below):

```go
type Guild struct {
	Game        string // retail or classic
	RealmSlug   string // realm slug, that may be used with any wow-related websites
	Progress    map[string]string // raid progress
	Alts        []Alt // alts/twings matching
	Name        string // guild name
	Vanity      string // guild url
	Realm       string // WarcraftLogs realm slug (do not use it, it's awful)
	Region      string // eu, us, etc.
	Attendance  Attendance // attendance filters configuration
	Members     GuildMember // guild roster
}

// Alt represents list of all characters of one player
type Alt struct {
	Main string
	Alts []string
}

// Attendance configuration for guild
type Attendance struct {
	MaxReports int        // output reports
	Raids      []int64    // 26 - Castle Nathria,  1000x - classic
	Bench      []string   // list of "autobench" chars
	Ignore     struct {
		Threshold float64 // Ignore chars with attendance lower than that number
		Modes     []int64 // Ignore bosses difficulty (lfr, normal, etc)
		Days      []int64 // Ignore reports, recored in following days. 0 - Sunday..
		Date      string  // Ignore reports before that date
		NonGuild  bool    // ignore PUGs/non-guild chars
	}
}

type GuildMember struct {
	Name  string // character name
	Level uint8  // character level
	Rank  uint8  // character guild rank
}
```

#### Example

Info about [Совет Тирисфаля](/tirisfal) guild

```bash
curl {{ site.url }}/api/tirisfal/guild.json
```

Response is too big to post it here, so just open it in browser: [Совет Тирисфаля JSON info]({{ site.url }}/api/tirisfal/guild.json)
</div>
