---
title: Guild progress updater
layout: page
category: help
description: What is autoupdater, how it works and how to use it.
---

<div class="transparent-bg" markdown="1">
WoW Guild Tools has a special system, that will update your guild progress automatically.

### What sites supported?

**Retail**: Raider.io, Wowprogress, WarcraftLogs

**Classic**: WarcraftLogs _other services does not support WoW Classic_

### How often guilds sent to update queue

Every hour.
If you want to check when you guild was updated last time, check the "updated X minutes ago" on guild page (in bottom right corner of page header)

### What should I do to start using it?

Register your [account](/account) and add your guild, after that your guild will be automatically updated, no additional configuration required.
</div>
