class Guild {

    // Replace timestamp with text "updated X mins ago"
    setUpdatedText() {
        const element = document.getElementById('updated');
        let text = '';
        const current = new Date();
        const updatedAt = new Date(element.innerHTML);
        const diff = (((current.getTime() + (1 * 60000)) - updatedAt.getTime()) / 1000);
        const day_diff = Math.floor(diff / 86400);
        const unit = {
            now: 'Now', minute: '1 min', minutes: ' mins', hour: '1 hr', hours: ' hrs',
            day: 'Yesterday', days: ' days', week: '1 week', weeks: ' weeks'
        };

        if (day_diff == 0){
            if (diff < 60) text = unit.now;
            else if (diff < 120) text = unit.minute;
            else if (diff < 3600) text = Math.floor(diff / 60) + unit.minutes;
            else if (diff < 7200) text = unit.hour;
            else if (diff < 86400) text = Math.floor(diff / 3600) + unit.hours;
            else return '';
        } else if (day_diff == 1) text = unit.day;
        else if (day_diff < 7) text = day_diff + unit.days;
        else if (day_diff == 7) text = unit.week;
        else if (day_diff > 7) text = Math.ceil(day_diff / 7) + unit.weeks;
        element.innerHTML = 'updated ' + text + ' ago';
    }

    // Load raid progress from raider.io if it's not provided from WGT
    setRaidProgress(guild, realm, region) {
        try{
            fetch('https://raider.io/api/v1/guilds/profile?region=' + region + '&realm=' + realm + '&name=' + guild + '&fields=raid_progression',
                { mode: 'cors', credentials: 'omit', redirect: 'follow', referrerPolicy: 'origin-when-cross-origin', headers: {
                    'Content-Type': 'application/json'
                }}).then((response) => {
                    if (!response.ok) {
                        console.error('[raider.io] request has been failed.');
                        return {};
                    }
                    return response.json();
                }).then((data) => {
                    if (!data) return;
                    const rp = data.raid_progression;
                    for (const raid in rp) {
                        if (!rp.hasOwnProperty(raid)) continue;
                        let element = document.getElementById('rp-' + raid);
                        if (!element) continue;
                        element.innerHTML = rp[raid].summary;
                    }
                    document.getElementById('raid-progress').style.display = 'flex';
                }).catch((error) => console.error('[raider.io] fetch:', error));
        } catch (error) {
            console.error('[raider.io] failed:', error);
        }
    }

    // Set ajax load on click of history's buttons
    setHistoryListeners() {
        const buttons = document.getElementsByClassName('show-raid-attendance');
        for(const button of buttons) {
            button.addEventListener('click', function (event) {
                event.preventDefault();
                const targetPath = event.target.getAttribute('data-id');
                const element = document.getElementById('attendance-' + targetPath);
                if (element.innerHTML.length) element.style.display = (element.style.display != 'block') ? 'block' : 'none';
                else {
                    let url = window.location.href;
                    if(url.indexOf('#') !== -1) url = url.substring(0, url.indexOf('#'));
                    url+= (url.slice(-1) == '/' ? targetPath : '/' + targetPath)
                    fetch(url).then((response) => { return response.text() })
                        .then((text) => {
                            element.innerHTML = text;
                            sorttable.makeSortable(element.getElementsByTagName('table')[0]);
                            element.style.display = 'block';
                        })
                        .catch((error) => console.error('[attendance] fetch:', error));
                }
            }, { capture: true });
        }
    }

    // Set css class by attendance percent
    setAttendanceColor() {
        const tdPulls = document.querySelectorAll('td.hit');
        for(const td of tdPulls) {
            if(td.innerHTML.indexOf('/') !== -1) {
                const values = td.innerHTML.split('/');
                const pullsPresent = parseInt(values[0]);
                const pullsTotal = parseInt(values[1]);
                const attendance = (pullsPresent / pullsTotal) * 100;
                td.classList.add(this.getAttendanceClass(attendance));
                if (attendance == 0 && pullsPresent == 0 && pullsTotal != 0) {
                    td.classList.add('bench');
                } else {
                    td.classList.add('present');
                }
            }
        }
    }

    // Get attendance css class name
    getAttendanceClass(attendance) {
        if(attendance <= 58) return 'att58';
        else if(attendance <= 64) return 'att64';
        else if(attendance <= 68) return 'att68';
        else if(attendance <= 72) return 'att72';
        else if(attendance <= 76) return 'att76';
        else if(attendance <= 80) return 'att80';
        else if(attendance <= 82) return 'att82';
        else if(attendance <= 87) return 'att87';
        else if(attendance <= 90) return 'att90';
        else if(attendance <= 97) return 'att97';
        else if(attendance > 97) return 'att100';
    }

    setBenches(guildId) {
        app.isGuildModificationAllowed(guildId).then(allowed => {
            if(!allowed) return;
            app.loadGuildBenches(guildId).then(benches => {
                for(const bench of benches) {
                    const td = document.querySelectorAll('td[data-report="' + bench.report_id + '"][data-character="' + bench.character + '"]')[0] ?? null;
                    if(!td) continue;
                    td.classList.add('bench');
                    td.dataset.bench = bench._id;
                }
                document.getElementById('data_note').style.display = 'block';
                guild.setBenchListeners(guildId);
            });
        });
    }

    setBenchListeners(guildId) {
        const present = 'present';
        const bench = 'bench';
        for(let td of document.getElementsByTagName('td')) {
            if(!td.dataset.report || !td.dataset.character) continue;
            td.addEventListener('click', async function(event) {
                console.log(td);
                if(this.classList.contains(present)) return;
                if(this.classList.contains(bench)) {
                    app.removeGuildBench(this.dataset.bench);
                    this.classList.remove(bench);
                    this.dataset.bench = null;
                    return;
                } else {
                    this.classList.add(bench);
                    let result = await app.createGuildBench(guildId, this.dataset.report, this.dataset.character);
                    this.dataset.bench = result.insertedId;
                    return;
                }
            });
        }
    }
}

const guild = new Guild();
