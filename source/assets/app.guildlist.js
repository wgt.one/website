class GuildList {

    init() {
        app.loadGuilds()
            .then(guilds => {
                if (guilds.length > 0) guildlist.show(guilds);
                else guildlist.showSteps();
                app.loader(false);
            })
            .catch(err => { console.error(err); app.loader(false); });
    }

    show(guilds = []) {
        const table = document.getElementById('account-guilds');
        const tbody = table.getElementsByTagName('tbody')[0];
        guilds.map(guild => {
            const row = tbody.insertRow();
            const site = 'https://' + window.location.host + '/';
            const url = site + (guild.vanity ?? '');
            const api = site + 'api/' + (guild.vanity ?? '');
            if(guild.hasOwnProperty('vanity') && guild.vanity) {
                guild.name = '<a href="' + url + '" target="_blank">' + guild.name + '</a>';
            } else {
                guild.name = '<b>' + guild.name + '</b> <i>it may take up to 1 hour after guild creation</i>';
            }
            row.innerHTML = '<td>' + guild.name + '</td><td>' + guild.game + '</td><td>' + guild.region + '-' + guild.realm + '</td>'
                + '<td><a href="' + api + '/guild.json">guild.json</a></td>'
                + '<td><a href="/account/guild/#' + guild._id + '">edit</a></td>';
        });
        table.classList.remove('hidden');
    }

    showSteps() {
        const element = document.getElementById('account-steps');
        element.classList.remove('hidden');
    }
}

const guildlist = new GuildList();
