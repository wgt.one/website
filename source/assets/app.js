class App {
    get realm() {
        if(!this.realmInstance) {
            this.realmInstance = new Realm.App({ id: 'wgt-picot' });
        }
        return this.realmInstance;
    }

    getCollection(name) {
        const mongo = this.realm.services.mongodb('mongodb-atlas');
        return mongo.db('wgt').collection(name);
    }

    loader(enable = true, id = 'container-main') {
        const container = document.getElementById(id);
        enable ? container.classList.add('loader') : container.classList.remove('loader');
    }

    redirect(uri) {
        window.location.href = uri;
    }

    get user() {
        return this.realm.currentUser;
    }

    login(email, password) {
        const credentials = Realm.Credentials.emailPassword(email, password);
        return this.realm.logIn(credentials);
    }

    async logout() {
        if(app.user) {
            await this.realm.currentUser.logOut();
        }
        return;
    }

    register(email, password) {
        return this.realm.emailPasswordAuth.registerUser(email, password);
    }

    resetPasswordRequest(email) {
        return this.realm.emailPasswordAuth.sendResetPasswordEmail(email);
    }

    resetPassword(token, tokenId, password) {
        return this.realm.emailPasswordAuth.resetPassword(token, tokenId, password);
    }

    loadGuild(guildId) {
        guildId = new BSON.ObjectId(guildId);
        const collection = this.getCollection('guilds');
        return collection.findOne({ _id : guildId });
    }

    // load current user's guilds
    loadGuilds() {
        const collection = this.getCollection('guilds');
        return collection.find();
    }

    createGuild(data) {
        const collection = this.getCollection('guilds');
        data.user_id = this.user.id;
        return collection.insertOne(data);
    }

    updateGuild(data) {
        const collection = this.getCollection('guilds');
        data._id = new BSON.ObjectId(data._id);
        data.user_id = this.user.id;
        return collection.updateOne({_id: data._id}, {'$set': data}, {upsert: false});
    }

    async isGuildModificationAllowed(currentId) {
        if(!this.user) {
            return false;
        }
        let guild = await this.loadGuild(currentId);
        if(guild.user_id == this.user.id) {
            return true;
        }

        return false;
    }

    loadGuildBenches(guildId) {
        const collection = this.getCollection('benches');
        return collection.find({guild_id: guildId, status: 'active'});
    }

    createGuildBench(guildId, reportId, character) {
        const collection = this.getCollection('benches');
        return collection.insertOne({
            user_id: this.user.id,
            report_id: reportId,
            guild_id: guildId,
            character: character,
            status: 'active'
        });
    }

    removeGuildBench(benchId) {
        const collection = this.getCollection('benches');
        return collection.updateOne(
            {_id: new BSON.ObjectId(benchId)},
            {'$set': {status: 'inactive'}},
            {upsert: false});
    }
}

const app = new App();
