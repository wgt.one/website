class GuildForm {

    init() {
        app.loader(true);
        this.initFormListeners();
        if(this.id) {
            app.loadGuild(this.id)
                .then(guild => guildform.formdata = guild)
                .catch(err => guildform.error(err.message));
        } else {
            guildform.formdata = null;
            app.loader(false);
        }
    }

    error(show = true, text = '') {
        const element = document.getElementById('form-error');
        element.innerHTML = text;
        element.style.display = show ? 'block' : 'none';
        try {
            document.getElementById('guild-create').removeAttribute('disabled');
        } catch {}
        try {
            document.getElementById('guild-update').removeAttribute('disabled');
        } catch {}
        app.loader(false);
    }

    get isError() {
        const element = document.getElementById('form-error');
        return (element.style.display == 'block');
    }

    // Get guild id from url hash
    get id() {
        if(!this.guildId) this.guildId = window.location.hash.substr(1);
        return this.guildId;
    }

    // Get form data
    get formdata() {
        let data = {attendance: {ignore:{}}, alts: []};
        const form = document.getElementById('guild');
        const formdata = new FormData(form);
        data.name = document.getElementsByName('name')[0].value;
        data.game = document.getElementsByName('game')[0].value;
        data.realm = document.getElementsByName('realm')[0].value;
        data.region = document.getElementsByName('region')[0].value;
        data.purge_cache = (new Date()).toISOString().split('T')[0];

        const WCLv1key = document.getElementsByName('integration[warcraftlogs][key]')[0].value;
        const WCLv2ID = document.getElementsByName('integration[warcraftlogs][client_id]')[0].value;
        const WCLv2Secret = document.getElementsByName('integration[warcraftlogs][client_secret]')[0].value;
        if(WCLv1key != "") {
            if(!data.hasOwnProperty('integration')) {
                data.integration = {};
                if(!data.integration.hasOwnProperty('warcraftlogs')) data.integration.warcraftlogs = {};
            }
            this.validateWCLv1Key(WCLv1key).catch(err => guildform.error(true, err.message));

            data.integration.warcraftlogs.key = WCLv1key;
        }

        if(WCLv2ID != "" && WCLv2Secret != "") {
            if(!data.hasOwnProperty('integration')) {
                data.integration = {};
                if(!data.integration.hasOwnProperty('warcraftlogs')) data.integration.warcraftlogs = {};
            }
            data.integration.warcraftlogs.client_id = WCLv2ID;
            data.integration.warcraftlogs.client_secret = WCLv2Secret;
        }

        const blizzardClientID = document.getElementsByName('integration[blizzard][client_id]')[0].value;
        const blizzardClientSecret = document.getElementsByName('integration[blizzard][client_secret]')[0].value;
        if(blizzardClientID != "" && blizzardClientSecret != "") {
            if(!data.hasOwnProperty('integration')) {
                data.integration = {};
                if(!data.integration.hasOwnProperty('blizzard')) data.integration.blizzard = {};
            }
            data.integration.blizzard.client_id = blizzardClientID;
            data.integration.blizzard.client_secret = blizzardClientSecret;
        }

        const bench = document.getElementsByName('attendance[bench]')[0].value.split("\n");
        if(bench && bench.length >= 1 && bench[0] !== "") {
            data.attendance.bench = bench;
        }

        const raids = Array.from(document.getElementsByName('attendance[raids]')[0].selectedOptions).map(option => option.value);
        const modes = Array.from(document.getElementsByName('attendance[ignore][modes]')[0].selectedOptions).map(option => option.value);
        const days = Array.from(document.getElementsByName('attendance[ignore][days]')[0].selectedOptions).map(option => option.value);

        data.attendance.raids = this.arrayParseInt(raids);
        data.attendance.maxReports = parseInt(document.getElementsByName('attendance[maxReports]')[0].value, 10);

        data.attendance.ignore.date = document.getElementsByName('attendance[ignore][date]')[0].value;
        data.attendance.ignore.threshold = parseFloat(document.getElementsByName('attendance[ignore][threshold]')[0].value);
        data.attendance.ignore.modes = this.arrayParseInt(modes);
        data.attendance.ignore.days = this.arrayParseInt(days);
        data.attendance.ignore.nonguild = document.getElementsByName('attendance[ignore][nonguild]')[0].checked;

        // Get twinks/alts and convert them to required format
        for(const entry of formdata.entries()) {
            if(entry[0].indexOf('alts[') == -1) continue;
            const [key, value] = entry;
            const index = key.match(/\[(.*?)\]/)[1];
            const type = key.match(/main|alt/g)[1];
            if(!data.alts[index]) {
                data.alts[index] = {main: '', alts: []};
            }
            switch(type) {
                case 'main':
                    data.alts[index].main = value;
                    break;
                case 'alt':
                    data.alts[index].alts.push(value);
                    break;
            }
        }
        return data;
    }

    set guildnote(uri = null) {
        const site = 'https://' + window.location.host + '/';
        const urls = document.getElementsByClassName('guild-url');
        const noteNew = document.getElementById('guild-note-new');
        const noteVanity = document.getElementById('guild-note-vanity');
        const noteNoVanity = document.getElementById('guild-note-novanity');

        for(const element of urls) {
            element.setAttribute('href', site + uri);
            element.innerHTML = site + uri;
        }

        if(uri === null) {
            noteNew.classList.remove('hidden');
            return;
        }

        if(uri.startsWith('retail-') || uri.startsWith('classic-')) {
            noteNoVanity.classList.remove('hidden');
            return;
        }

        noteVanity.classList.remove('hidden');
    }

    // Set form data from database
    set formdata(data) {
        if(data === null) {
            this.formgame = 'retail';
            this.guildnote = null;
            document.getElementsByName('attendance[maxReports]')[0].value = 10;
            document.getElementsByName('attendance[ignore][threshold]')[0].value = 10;
            this.multiselect(document.getElementsByName('attendance[raids]')[0], [], true);
            this.addAltsLine();
            document.getElementById('guild-update').remove();
            return;
        } else {
            this.guildnote = data.hasOwnProperty('vanity') ? data.vanity : null;
            document.getElementById('guild-create').remove();
        }
        this.formgame = data.game;
        document.getElementsByName('name')[0].value = data.name;
        document.getElementsByName('game')[0].value = data.game;
        document.getElementsByName('region')[0].value = data.region;
        document.getElementsByName('realm')[0].value = data.realm;

        if(data.hasOwnProperty('integration')) {
            if(data.integration.hasOwnProperty('warcraftlogs')) {
                const v1key = data.integration.apikey ?? data.integration.warcraftlogs.key ?? '';
                document.getElementsByName('integration[warcraftlogs][key]')[0].value = v1key;

                const v2id = data.integration.warcraftlogs.client_id ?? '';
                const v2secret = data.integration.warcraftlogs.client_secret ?? '';
                document.getElementsByName('integration[warcraftlogs][client_id]')[0].value = v2client_id;
                document.getElementsByName('integration[warcraftlogs][client_secret]')[0].value = v2client_secret;
            }
            if(data.integration.hasOwnProperty('blizzard')) {
                const blizzardClientID = data.integration.blizzard.client_id ?? '';
                const blizzardClientSecret = data.integration.blizzard.client_secret ?? '';
                document.getElementsByName('integration[blizzard][client_id]')[0].value = blizzardClientID;
                document.getElementsByName('integration[blizzard][client_secret]')[0].value = blizzardClientSecret;
            }
        }

        if(data.attendance.hasOwnProperty('bench')) {
            document.getElementsByName('attendance[bench]')[0].value = data.attendance.bench.join("\n");
        }
        document.getElementsByName('attendance[maxReports]')[0].value = data.attendance.maxReports;
        this.multiselect(document.getElementsByName('attendance[raids]')[0], data.attendance.raids);

        document.getElementsByName('attendance[ignore][threshold]')[0].value = data.attendance.ignore.threshold;
        document.getElementsByName('attendance[ignore][date]')[0].value = data.attendance.ignore.date;
        this.multiselect(document.getElementsByName('attendance[ignore][modes]')[0], data.attendance.ignore.modes);
        this.multiselect(document.getElementsByName('attendance[ignore][days]')[0], data.attendance.ignore.days);
        document.getElementsByName('attendance[ignore][nonguild]')[0].checked = data.attendance.ignore.nonguild ?? false;

        if(data.alts.length > 0) {
            data.alts.map((item, index) => guildform.addAltsLine(index, item));
        }

        app.loader(false);
    }

    addAlt(button, block){
        const tplInput = document.getElementById('tpl-alt-input');
        const tplButton = document.getElementById('tpl-alt-input-button');
        const nextName = block.getElementsByTagName('input')[1].name.replace('[main]', '');
        for(const node of tplInput.children) {
            const clone = node.cloneNode(true);
            clone.setAttribute('name', clone.name.replace('alts[0]', nextName));
            block.append(clone);
        }

        for(const node of tplButton.children) {
            const clone = node.cloneNode(true);
            block.append(clone);
        }

        button.remove();
        this.initAltListeners(block);

        return block;
    }

    // Create new alts' line element
    createAltsLine() {
        const tpl = document.getElementById('tpl-alt-line');
        for(const node of tpl.childNodes) {
            if(node.childNodes.length == 0) continue;
            const element = node.cloneNode(true);
            this.initAltListeners(element);
            return element;
        }
    }

    initAltListeners(element = document) {
        for(const button of element.getElementsByClassName('add-alt')) {
            button.addEventListener('click', function(event) {
                event.preventDefault();
                guildform.addAlt(event.target, event.target.parentElement);
            });
        }

        for(const button of element.getElementsByClassName('remove-alt')) {
            button.addEventListener('click', function(event) {
                event.preventDefault();
                event.target.parentElement.remove();
            });
        }
    }

    addAltsLine(nextId = 0, item = {}){
        const line = this.createAltsLine();
        const block = document.getElementById('guild-alts');
        const children = block.getElementsByClassName('alt-field');
        if(nextId == 0 && children.length > 0) {
            nextId = parseInt(children[children.length - 1].name.match(/\d+/)[0], 10) + 1;
        }
        let altId = 0;
        for(const sub of line.getElementsByClassName('alt-field')) {
            if(nextId != 0) sub.setAttribute('name', sub.name.replace('[0]', '[' + nextId + ']'));
        }
        if(!item.hasOwnProperty('alts') || item.alts.length == 0) {
            block.append(line);
            return;
        }
        let addAltButton = line.getElementsByClassName('add-alt')[0];
        for(const alt of item.alts) {
            addAltButton = guildform
                .addAlt(addAltButton, addAltButton.parentElement)
                .getElementsByClassName('add-alt')[0];
        }
        altId = 0;
        for(const sub of line.getElementsByClassName('alt-field')) {
            if(!sub.name) continue;
            if(nextId != 0) sub.setAttribute('name', sub.name.replace('[0]', '[' + nextId + ']'));
            let type = sub.name.substr(7);
            switch(type) {
                case '[main]':
                case '][main]':
                    sub.value = item.main;
                    break;
                case '[alts][]':
                case '][alts][]':
                    sub.value = item.alts[altId];
                    altId++;
                    break;
            }
        }
        block.append(line);
    }

    // Retail and classic game versions have some differencies
    set formgame(game = 'retail') {
        const elements = [
            document.getElementsByName('attendance[ignore][modes]')[0],
            document.getElementById('ignoredModesLabel'),
        ];
        const elementsInline = [
            document.getElementsByName('attendance[ignore][nonguild]')[0],
            document.getElementById('ignoredNonGuildLabel'),
        ];
        let retailRaids = [];
        let classicRaids = [];
        let raidOptions = Array.from(document.getElementsByName('attendance[raids]')[0].getElementsByTagName('option'));
        raidOptions.map(raid => {
            if(raid.value < 1000) retailRaids.push(raid);
            else classicRaids.push(raid);
        });
        if(game == "retail") {
            elements.map(element => element.style.display = 'block');
            elementsInline.map(element => element.style.display = 'inline-block');
            classicRaids.map(raid => raid.setAttribute('disabled', true));
            retailRaids.map(raid => raid.removeAttribute('disabled'));
        } else {
            elements.map(element => element.style.display = 'none');
            elementsInline.map(element => element.style.display = 'none');
            classicRaids.map(raid => raid.removeAttribute('disabled'));
            retailRaids.map(raid => raid.setAttribute('disabled', true));
        }
    }

    initFormListeners() {
        document.getElementById('guild-create').addEventListener('click', function(event){
            event.preventDefault();
            this.setAttribute('disabled', true);
            guildform.save();
        });

        document.getElementById('guild-update').addEventListener('click', function(event){
            event.preventDefault();
            this.setAttribute('disabled', true);
            guildform.save();
        });

        document.getElementsByName('game')[0].addEventListener('change', function(event) {
            guildform.formgame = this.value;
        });

        document.getElementsByClassName('add-alt-line')[0].addEventListener('click', function(event){
            guildform.addAltsLine();
        });
    }

    save() {
        const form = document.getElementById('guild');
        if(!form.checkValidity()) {
            this.error(true, 'Entered data is not valid. Seems you missed some field(-s).');
            return;
        }
        let data = this.formdata;
        if(app.isError) {
            return;
        }
        if(this.id) {
            data._id = new BSON.ObjectId(this.id);
            app.updateGuild(data)
                .then(result => app.redirect('/account/'))
                .catch(err => guildform.error(true, err.message));
        } else {
            app.createGuild(data)
                .then(result => app.redirect('/account/'))
                .catch(err => guildform.error(true, err.message));
        }
    }

    // ParseInt for array
    arrayParseInt(iterable) {
        let array = Array.from(iterable);
        return array.map(value => parseInt(value, 10));
    }

    // Select multiple options in <select multiple> element
    multiselect(element, values, all = false) {
        for (let idx = 0; idx < element.options.length; idx++) {
            element.options[idx].selected = (all) ? true : values.indexOf(parseInt(element.options[idx].value, 10)) > -1;
        }
    }

    // Check if Warcraft Logs API key works as expected
    async validateWCLv1Key(apikey) {
        const response = await fetch('https://www.warcraftlogs.com:443/v1/classes?api_key=' + apikey);
        switch(response.status) {
            case 401:
                throw 'WarcraftLogs API Key does not exists. May be you copied only part of the key';
                break;
            case 403:
                throw 'WarcraftLogs API Key has no name. Please, check the (?) link of the Api Key field for more information';
                break;
        }
    }
}

const guildform = new GuildForm();
