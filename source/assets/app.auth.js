class Auth {

    init() {
        try {
            document.getElementById('auth-register')
                .addEventListener('click', function (event){ auth.handle(event, 'register'); });
        } catch (error) {}
        try {
            document.getElementById('auth-reset')
                .addEventListener('click', function (event){ auth.handle(event, 'reset'); });
        } catch (error) {}
        try {
            document.getElementById('auth-login')
                .addEventListener('click', function (event){ auth.handle(event, 'login'); });
        } catch (error) {}
        try {
            document.getElementById('auth-reset-request')
                .addEventListener('click', function (event){ auth.handle(event, 'reset-request'); });
        } catch (error) {}
        app.loader(false);
    }

    error(show = true, text = '') {
        const element = document.getElementById('form-error');
        element.innerHTML = text;
        element.style.display = show ? 'block' : 'none';
        app.loader(false);
    }

    handle(event, type) {
        event.preventDefault();
        this.error(false);
        const form = document.getElementById('auth');
        const data = new FormData(form);
        if(!form.checkValidity()) {
            this.error(true, 'Form field(-s) is empty');
            return;
        }

        switch(type) {
            case 'register':
                app.register(data.get('email'), data.get('password'))
                    .then(result => auth.handle(event, 'login') )
                    .catch(err => auth.error(true, err.message));
                break;
            case 'login':
                app.login(data.get('email'), data.get('password'))
                    .then(result => app.redirect('/account/'))
                    .catch(err => auth.error(true, err.message));
                break;
            case 'reset':
                const params = new URLSearchParams(window.location.search);
                if(!params.has('token') || !params.has('tokenId')) {
                    this.error(true, 'Your url does not contain required params. Please, check your email');
                    return;
                }
                app.resetPassword(params.get('token'), params.get('tokenId'), data.get('password'))
                .then(result => app.redirect('/account/auth/'))
                .catch(err => auth.error(true, err.message));
                break;
            case 'reset-request':
                app.resetPasswordRequest(data.get('email'))
                    .then(result => app.redirect('/'))
                    .catch(err => auth.error(true, err.message));
                break;
            default:
                this.error(true, '418 - You are teapot!');
        }
    }
}

const auth = new Auth();
